#!/usr/bin/env bash

export ZONE=central
export IP=192.168.24.2
export NETMASK=24
# I use eth0 for connectivity to the VM.
export INTERFACE=eth1
# The 192.168.100.1 address is my virsh NAT'ed interface that
# provides DHCP for this node's eth0 interface. DHCP also
# supplies a default route for the control place.
export DNS_SERVERS=192.168.100.1
# These NTP servers ensure things work inside or outside the RH network.
export NTP_SERVERS=clock.corp.redhat.com

sudo hostnamectl set-hostname standalone-${ZONE}.localdomain

cat <<EOF > $HOME/standalone_parameters.yaml
parameter_defaults:
  CertmongerCA: local
  CloudName: $IP
  ContainerCli: podman
  ControlPlaneStaticRoutes: []
  Debug: true
  DeploymentUser: $USER
  DnsServers: $DNS_SERVERS
  NtpServer: $NTP_SERVERS
  # needed for vip & pacemaker
  KernelIpNonLocalBind: 1
  DockerInsecureRegistryAddress:
  - $IP:8787
  NeutronPublicInterface: $INTERFACE
  # domain name used by the host
  NeutronDnsDomain: localdomain
  # re-use ctlplane bridge for public net
  NeutronBridgeMappings: datacentre:br-ctlplane
  NeutronPhysicalBridge: br-ctlplane
  # enable to force metadata for public net
  #NeutronEnableForceMetadata: true
  NovaComputeLibvirtType: qemu
  StandaloneEnableRoutedNetworks: false
  StandaloneHomeDir: $HOME
  StandaloneLocalMtu: 1400
  # Needed if running in a VM
  StandaloneExtraConfig:
    oslo_messaging_notify_use_ssl: false
    oslo_messaging_rpc_use_ssl: false
    tripleo::firewall::firewall_rules:
      '300 allow ssh access':
        port: 22
        proto: tcp
        destination: 192.168.100.0/24
        action: accept
  SELinuxMode: permissive
EOF

if [ ! -f $HOME/containers-prepare-parameters.yaml ]; then
  openstack tripleo container image prepare default \
    --output-env-file $HOME/containers-prepare-parameters.yaml
fi

if [ ! -f $HOME/ceph_parameters.yaml ]; then
  sudo dd if=/dev/zero of=/var/lib/ceph-osd.img bs=1 count=0 seek=7G
  sudo losetup /dev/loop3 /var/lib/ceph-osd.img
  sudo mkdir /root/ceph_ansible_fetch

  cat <<EOF > $HOME/ceph_parameters.yaml
parameter_defaults:
  CephAnsibleDisksConfig:
    devices:
      - /dev/loop3
    journal_size: 1024
  CephAnsibleExtraConfig:
    osd_scenario: collocated
    osd_objectstore: filestore
    cluster_network: 192.168.24.0/24
    public_network: 192.168.24.0/24
  CephPoolDefaultPgNum: 32
  CephPoolDefaultSize: 1
  CephAnsiblePlaybookVerbosity: 3
  LocalCephAnsibleFetchDirectoryBackup: /root/ceph_ansible_fetch
EOF
fi

if [[ ! -d ~/templates ]]; then
    ln -s /usr/share/openstack-tripleo-heat-templates ~/templates
fi

cat <<EOF > $HOME/central_parameters.yaml
parameter_defaults:
  GlanceBackend: swift
EOF

sudo pkill -9 heat-all # Remove any previously running heat processes
sudo openstack tripleo deploy \
  --templates ~/templates \
  --local-ip=$IP/$NETMASK \
  -e ~/templates/environments/standalone/standalone-tripleo.yaml \
  -e ~/templates/environments/ceph-ansible/ceph-ansible.yaml \
  -r ~/templates/roles/Standalone.yaml \
  -e ~/containers-prepare-parameters.yaml \
  -e ~/standalone_parameters.yaml \
  -e ~/ceph_parameters.yaml \
  -e ~/central_parameters.yaml \
  --output-dir $HOME \
  --keep-running \
  --standalone $@
