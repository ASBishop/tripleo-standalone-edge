#!/usr/bin/env bash

if [ -n "$1" ]; then
  SITE_NUM=$1
  shift
else
  read -p "Edge site number (e.g. 1..9): " SITE_NUM
fi

# Simple validation test supports up to 9 Edge sites
if ! [[ "$SITE_NUM" =~ ^[1-9]$ ]] ; then
  echo "error: \"$SITE_NUM\" is not a valid site number (must from 1-9)." >& 2
  exit 1
fi

export ZONE="edge${SITE_NUM}"
export IP=192.168.24.$((2 + $SITE_NUM))
export NETMASK=24
# I use eth0 for connectivity to the VM.
export INTERFACE=eth1
# The 192.168.100.1 address is my virsh NAT'ed interface that
# provides DHCP for this node's eth0 interface. DHCP also
# supplies a default route for the control place.
export DNS_SERVERS=192.168.100.1
# These NTP servers ensure things work inside or outside the RH network.
export NTP_SERVERS=clock.corp.redhat.com

sudo hostnamectl set-hostname standalone-${ZONE}.localdomain

cat <<EOF > $HOME/standalone_parameters.yaml
parameter_defaults:
  CertmongerCA: local
  CloudName: $IP
  ContainerCli: podman
  ControlPlaneStaticRoutes: []
  Debug: true
  DeploymentUser: $USER
  DnsServers: $DNS_SERVERS
  NtpServer: $NTP_SERVERS
  # needed for vip & pacemaker
  KernelIpNonLocalBind: 1
  DockerInsecureRegistryAddress:
  - $IP:8787
  NeutronPublicInterface: $INTERFACE
  # domain name used by the host
  NeutronDnsDomain: localdomain
  # re-use ctlplane bridge for public net
  NeutronBridgeMappings: datacentre:br-ctlplane
  NeutronPhysicalBridge: br-ctlplane
  # enable to force metadata for public net
  #NeutronEnableForceMetadata: true
  NovaComputeLibvirtType: qemu
  StandaloneEnableRoutedNetworks: false
  StandaloneHomeDir: $HOME
  StandaloneLocalMtu: 1400
  # Needed if running in a VM
  StandaloneExtraConfig:
    oslo_messaging_notify_use_ssl: false
    oslo_messaging_rpc_use_ssl: false
    tripleo::firewall::firewall_rules:
      '300 allow ssh access':
        port: 22
        proto: tcp
        destination: 192.168.100.0/24
        action: accept
  SELinuxMode: permissive
EOF

if [ ! -f $HOME/containers-prepare-parameters.yaml ]; then
  openstack tripleo container image prepare default \
    --output-env-file $HOME/containers-prepare-parameters.yaml
fi

if [ ! -f $HOME/ceph_parameters.yaml ]; then
  sudo dd if=/dev/zero of=/var/lib/ceph-osd.img bs=1 count=0 seek=7G
  sudo losetup /dev/loop3 /var/lib/ceph-osd.img
  sudo mkdir /root/ceph_ansible_fetch

  cat <<EOF > $HOME/ceph_parameters.yaml
parameter_defaults:
  CephAnsibleDisksConfig:
    devices:
      - /dev/loop3
    journal_size: 1024
  CephAnsibleExtraConfig:
    osd_scenario: collocated
    osd_objectstore: filestore
    cluster_network: 192.168.24.0/24
    public_network: 192.168.24.0/24
  CephPoolDefaultPgNum: 32
  CephPoolDefaultSize: 1
  CephAnsiblePlaybookVerbosity: 3
  LocalCephAnsibleFetchDirectoryBackup: /root/ceph_ansible_fetch
EOF
fi

if [[ ! -d ~/templates ]]; then
    ln -s /usr/share/openstack-tripleo-heat-templates ~/templates
fi

cat <<EOF > $HOME/edge_parameters.yaml
resource_registry:
  OS::TripleO::Services::CACerts: OS::Heat::None
  OS::TripleO::Services::CinderApi: OS::Heat::None
  OS::TripleO::Services::CinderScheduler: OS::Heat::None
  OS::TripleO::Services::Clustercheck: OS::Heat::None
  OS::TripleO::Services::HAproxy: OS::Heat::None
  OS::TripleO::Services::Horizon: OS::Heat::None
  OS::TripleO::Services::Keystone: OS::Heat::None
  OS::TripleO::Services::Memcached: OS::Heat::None
  OS::TripleO::Services::MySQL: OS::Heat::None
  OS::TripleO::Services::NeutronApi: OS::Heat::None
  OS::TripleO::Services::NeutronDhcpAgent: OS::Heat::None
  OS::TripleO::Services::NovaApi: OS::Heat::None
  OS::TripleO::Services::NovaConductor: OS::Heat::None
  OS::TripleO::Services::NovaConsoleauth: OS::Heat::None
  OS::TripleO::Services::NovaIronic: OS::Heat::None
  OS::TripleO::Services::NovaMetadata: OS::Heat::None
  OS::TripleO::Services::NovaPlacement: OS::Heat::None
  OS::TripleO::Services::NovaScheduler: OS::Heat::None
  OS::TripleO::Services::NovaVncProxy: OS::Heat::None
  OS::TripleO::Services::OsloMessagingNotify: OS::Heat::None
  OS::TripleO::Services::OsloMessagingRpc: OS::Heat::None
  OS::TripleO::Services::Redis: OS::Heat::None
  OS::TripleO::Services::SwiftProxy: OS::Heat::None
  OS::TripleO::Services::SwiftStorage: OS::Heat::None
  OS::TripleO::Services::SwiftRingBuilder: OS::Heat::None

parameter_defaults:
  CinderRbdAvailabilityZone: $ZONE
  GlanceBackend: swift
  GlanceCacheEnabled: true
EOF

sudo openstack tripleo deploy \
  --templates ~/templates \
  --local-ip=$IP/$NETMASK \
  -e ~/templates/environments/standalone/standalone-tripleo.yaml \
  -e ~/templates/environments/ceph-ansible/ceph-ansible.yaml \
  -e ~/templates/environments/cinder-volume-active-active.yaml \
  -r ~/templates/roles/Standalone.yaml \
  -e ~/containers-prepare-parameters.yaml \
  -e ~/standalone_parameters.yaml \
  -e ~/ceph_parameters.yaml \
  -e ~/edge_parameters.yaml \
  -e ~/export_control_plane/passwords.yaml \
  -e ~/export_control_plane/endpoint-map.json \
  -e ~/export_control_plane/all-nodes-extra-map-data.json \
  -e ~/export_control_plane/extra-host-file-entries.json \
  -e ~/export_control_plane/global-config-extra-map-data.json \
  -e ~/export_control_plane/oslo.yaml \
  --output-dir $HOME \
  --standalone $@

if [ $? -eq 0 ]; then
   echo ""
   echo "Edge site $SITE_NUM (AZ \"$ZONE\") is using $IP"
fi
