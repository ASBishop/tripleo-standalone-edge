# tripleo-standalone-edge

This repository is used for local testing to deploy TripleO in Standalone for
the Edge Computing use case as described in the [Split Control
Plane](https://specs.openstack.org/openstack/tripleo-specs/specs/rocky/split-controlplane.html). It's
based on work by
[fultonj](https://gitlab.com/fultonj/tripleo-standalone-edge),
[slagle](https://gitlab.com/slagle/tripleo-standalone-edge) and
[EmilienM](https://gitlab.com/emacchi/tripleo-standalone-edge).

## Challenge

I would like to deploy OpenStack with TripleO but with remote
compute/ceph nodes (HCI). My remote compute/ceph nodes are on 
the Edge of the network where the workload is consumed by my
users.

## Architecture

```
+--------------------------+         +--------------------------+
|standalone|cpu|ceph|edge|1|         |standalone|cpu|ceph|edge|2|
+------------------------+-+         +-+------------------------+
                         |             |
                      +--+-------------+-+
                      |standalone|central|
                      +------------------+
```

In my setup I have three 8G 2vCPU Centos7 VMs on my dev machine (hypervisor).
Each has two NICs, with one (eth0) used for connectivity to the rest of the
world, and the other (eth1) used for the TripleO deployment. This 2-NIC model
is described in the docs as ["Example: 2 NIC, Using Compute with Tenant and
Provider Networks"](http://tripleo.org/install/containers_deployment/standalone.html#example-2-nic-using-compute-with-tenant-and-provider-networks).

## How to deploy

I'm using the [official
documentation](https://docs.openstack.org/tripleo-docs/latest/install/containers_deployment/standalone.html)
to deploy, but with custom environments established by the deployment
scripts.

### Step 1 - Bootstrap

Deploy the repository on each standalone host:

```bash
$ git clone https://gitlab.com/ASBishop/tripleo-standalone-edge.git
```

Then follow the documentation to deploy tripleo-repos, and install the
latest python-tripleoclient. The whole step 1 has to be done on every
standalone node.

Alternatively, you can try running [bootstrap.sh](bootstrap.sh).

### Step 2 - Deploy Central node

Login into the standalone central node and (if necessary) modify the
standalone-central.sh script to match the IP address and local interface of
the node. Then run the deployment script:

```bash
$ tripleo-standalone-edge/standalone-central.sh
```

Once the standalone-central is deployed export its information from
its Heat stack using [create-compute-stack-env.sh](create-compute-stack-env.sh),
which will produce a file called `export_control_plane.tar.gz` which 
you should then `scp` to the remote compute node on the edge.

```bash
$ tripleo-standalone-edge/create-compute-stack-env.sh
$ ls export_control_plane.tar.gz
export_control_plane.tar.gz
```

### Step 3 - Deploy a remote HCI node on the Edge

Login into the standalone edge node and unpack `export_control_plane.tar.gz`
in the home directory of the user which will deploy the compute node.
Modify the standalone-edge.sh script to match the IP address and local
interface of the node.

```bash
$ tar zxf export_control_plane.tar.gz
$ tripleo-standalone-edge/standalone-edge.sh
```

### Step 4 - (Optional) Deploy an HCI node on another Edge site

Repeat step 3, but modify the deployment parameters before invoking the
deployment script.
* Change the ZONE variable to specify another zone name (e.g. "edge2")
* Change the IP variable to specify another site address (192.168.24.4)

### Step 5 - Test the deployment

On the central node, authenticate to the openstack install and verify it can
see the services deployed at the edge node(s). This is a very quick, crude
test.

```bash
[stack@standalone ~]$ export OS_CLOUD=standalone
[stack@standalone ~]$ openstack volume service list
+------------------+----------------------+-------+---------+-------+----------------------------+
| Binary           | Host                 | Zone  | Status  | State | Updated At                 |
+------------------+----------------------+-------+---------+-------+----------------------------+
| cinder-scheduler | central              | nova  | enabled | up    | 2018-10-15T20:58:57.000000 |
| cinder-volume    | central@tripleo_ceph | nova  | enabled | up    | 2018-10-15T20:59:00.000000 |
| cinder-volume    | edge1@tripleo_ceph   | edge1 | enabled | up    | 2018-10-15T20:58:52.000000 |
| cinder-volume    | edge2@tripleo_ceph   | edge2 | enabled | up    | 2018-10-15T20:58:52.000000 |
+------------------+----------------------+-------+---------+-------+----------------------------+
[stack@standalone ~]$ openstack compute service list
+----+------------------+---------+----------+---------+-------+----------------------------+
| ID | Binary           | Host    | Zone     | Status  | State | Updated At                 |
+----+------------------+---------+----------+---------+-------+----------------------------+
|  1 | nova-scheduler   | central | internal | enabled | up    | 2018-10-15T20:59:26.000000 |
|  3 | nova-consoleauth | central | internal | enabled | up    | 2018-10-15T20:59:21.000000 |
|  4 | nova-conductor   | central | internal | enabled | up    | 2018-10-15T20:59:20.000000 |
|  5 | nova-compute     | central | nova     | enabled | up    | 2018-10-15T20:59:20.000000 |
|  7 | nova-compute     | edge1   | nova     | enabled | up    | 2018-10-15T20:59:28.000000 |
|  8 | nova-compute     | edge2   | nova     | enabled | up    | 2018-10-15T20:59:28.000000 |
+----+------------------+---------+----------+---------+-------+----------------------------+
[stack@standalone ~]$ openstack availability zone list
+-----------+-------------+
| Zone Name | Zone Status |
+-----------+-------------+
| internal  | available   |
| nova      | available   |
| edge1     | available   |
| edge2     | available   |
| nova      | available   |
| nova      | available   |
| nova      | available   |
+-----------+-------------+
```
### Discover Compute Node

```
[stack@standalone ~]$ sudo docker exec -it nova_api nova-manage cell_v2 discover_hosts --verbose
Found 2 cell mappings.
Skipping cell0 since it does not contain hosts.
Getting computes from cell 'default': 728b4d5b-362e-494f-ad6b-f73fdda78d06
Checking host mapping for compute host 'central': 05a22704-5a61-43dd-bf30-7717698d74fc
Checking host mapping for compute host 'edge1': b8a47a5e-79e8-482f-a75d-e6b788e33a2a
Creating host mapping for compute host 'edge1': b8a47a5e-79e8-482f-a75d-e6b788e33a2a
Checking host mapping for compute host 'edge2': c811d97c-2675-4892-9235-3b2fa939f982
Creating host mapping for compute host 'edge2': c811d97c-2675-4892-9235-3b2fa939f982
Found 2 unmapped computes in cell: 728b4d5b-362e-494f-ad6b-f73fdda78d06
[stack@standalone ~]$ openstack hypervisor list
+----+------------------------+-----------------+--------------+-------+
| ID | Hypervisor Hostname    | Hypervisor Type | Host IP      | State |
+----+------------------------+-----------------+--------------+-------+
|  1 | standalone.localdomain | QEMU            | 192.168.24.2 | up    |
|  2 | standalone.localdomain | QEMU            | 192.168.24.3 | up    |
|  3 | standalone.localdomain | QEMU            | 192.168.24.4 | up    |
+----+------------------------+-----------------+--------------+-------+
```

Notice the hypervisor hostnames are all the same. This is a side effect of not
updating the hostname for each node before deployment.

## TODO

1. Add glance-api on the edge node with a backend in the central zone
2. Testing, testing, testing...
