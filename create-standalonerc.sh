#!/usr/bin/env bash

if [ ! -e $HOME/undercloud-passwords.conf ]; then
    echo "Error: undercloud-passwords.conf is missing."
    echo "This script should only be run after deploying the central node."
    exit 1
fi

export OS_CLOUD=standalone

cat <<EOF > $HOME/standalonerc
export OS_AUTH_URL=$(openstack endpoint list --service identity --interface admin -f value -c URL)
export OS_AUTH_TYPE=password
export OS_USERNAME=admin
export OS_PASSWORD=$(awk '$1 == "undercloud_admin_password:" {print $2}' undercloud-passwords.conf)
export OS_PROJECT_NAME=admin
export COMPUTE_API_VERSION=1.1
export OS_USER_DOMAIN_NAME=Default
export OS_VOLUME_API_VERSION=3
export NOVA_VERSION=1.1
export OS_IMAGE_API_VERSION=2
export OS_PROJECT_DOMAIN_NAME=Default
export OS_IDENTITY_API_VERSION=3
export OS_NO_CACHE=True
export PYTHONWARNINGS="ignore:Certificate has no, ignore:A true SSLContext object is not available"
EOF
